VIKI
========

![viki_image](https://gitlab.com/vaisakh032/viki/-/raw/master/essentials/viki.png)

Viki is a personal assistant that can be deployed in your 
home, which will take care of different tasks. Currently in 
her development stage, different functionalities are added
to her capability sets.

Each functionality is currently a seperate progam that
strictly follows the criteria for it to be added and 
recognised as an viki capability.

This document will give a clear picture on the usage as 
well as the working of the systems. 

# Usage
## Deployment
For deployment of VIKI in your personal machine you will have to
install the following packages:

* Gstreamer packages
```
sudo apt-get install gstreamer1.0-tools
sudo apt-get install python-gst-1.0
```
* Nmap packages
```
sudo apt-get install nmap
sudo pip3 install python-nmap
```
* Bluepy packages
```
sudo apt-get install python3-pip libglib2.0-dev
sudo pip3 install bluepy
```
For the detailed installation of bluepy module, please follow this 
[link](https://github.com/IanHarvey/bluepy#installation)

>nmap module is required for network scanning of devices and the 
bluepy module is required for fetching nearby ble device (smart bands, beacons etc.) 

*  tabCompletion packages
```
sudo pip3 install tabCompletion
```

* ZMQ packages

```
sudo pip3 install zmq
```

## To Get started with VIKI
-----------------------------------------------------------------
To start viki, just run the ``viki.py``.
```
python3 viki.py
```
> You will have to give sudo privilages during run time.

### Commands 
| Module        | Command                           | Description      |
|---------------|-----------------------------------|------------------|
|daemon_handler |start daemon_handler               | Starts daemon handler with request to spawn all registered daemons (a.k.a modules)
|               |daemon_handler start *daemon_name* | start the daemon with the corresponding daemon name `[ Note: The daemon name does not auto-complete]`
|               |daemon_handler stop  *daemon_name* |stop the daemon with the corresponding daemon name `[ Note: The daemon name does not auto-complete]`
|               |daemon_handler status              | Gives the status and PID of daemon handler module.
|               |daemon_handler kill *daemon_name*  |force kill the daemon with the corresponding daemon name `[ Note: The daemon name does not auto-complete]`
|               |daemon_handler report              | Gives the current status of all registered daemons. 
|               |daemon_handler add_new-daemon      |
|scanner_daemon |scanner_daemon kill                | Kill scanner_daemon module. `[ Note: same as daemon_handler kill scanner_daemon ]`
|               |scanner_daemon status              | Gives the status and PID of scanner_daemon module. 
|               |scanner_daemon --forcescan=network | Request the scanner_daemon module to force scan for list of network occupants
|               |scanner_daemon --forcescan=ble     | Request the scanner_daemon module to force scan for list of nearby ble devices
|               |scanner_daemon report              | Gives the list of devices from both network and ble scan
|clock          |clock status                       | Gives the status and PID of clock module.
|               |clock now                          | Gives the current date and time.
|               |clock set alarm                    | Allows to set alarm.
|               |time                               | Gives the current time.
|               |clock kill                         | Kill clock module. `[ Note: same as daemon_handler kill clock ]`
|viki_speak     |viki_speak status                  | Gives the status and PID of viki_speak module.
|               |viki_speak greeting                | Plays the audio `"Greetings earthlings. How are you?"`
|               |viki_speak kill                    | Kill viki_speak module. `[ Note: same as daemon_handler kill viki_speak ]`
|event_handler  |event_handler status               | Gives the status and PID of event_handler module.
|               |event_handler network_occupants    | Gives the list of network occupants. Lists the names of saved devices.
|               |event_handler network_occupants --detailed | Gives the detailed list of network occupants.
|               |event_handler fetch_details        | Fetches details of unknown macs in network.
|               |event_handler kill                 | Kill event_handler module. `[ Note: same as daemon_handler kill event_handler ]`
| `Viki cli - generic commans`|exit                 | Exit from Viki.
## Present Capabilities
Viki is currently under development and her capabilities are added 
day by day. At present her capabilities include:

* Daemon Handler
* Speak Module
* People detection
* Clock
* Command Line Interface

### Daemon Handler:
The Daemon Handler module give viki the capability to handle registered
modules in viki. Each Module that are included is spawned and 
handled each time viki is invoked. Daemon Handler will handle 
the lifetime of each modules.

### Speak Module:
The speak module is responsible for all auditory feedback given to the user.
An underlying gstreamer player play respective audio files requested by 
each viki modules so as to give a full audio feedback to the user. 

### People Detection:
This capability is implemented with the macSacanner module. The macScanner 
module enables people detection through two main methods one, through scanning 
for devices in the network, two, by scanning for ble devices. At present, the 
ony limitation regarding network search is that the device has to be connected
to the home network. But development is undergoing on ESP support modules that 
are capable of fiding out all the devices with their wifi's on. 

### Clock:
The clock module is responsible for enabling clock functionalities to VIKI. 
It helps viki to handle alarms, reminder etc.

### Command Line Interface:
This interface helps the user to take control of different modules in viki.
Special commands are listed for accessing each functionalities. The CLI 
also provides tab to complete functionality just as in a UNIX terminal.
