import zmq
from viki_processes_ID import viki_process


def request_control(functionName):
    functionName("*#*#[REQUESTING CONTROL]*#*#")

def check__if_control_requested(message):
    if "*#*#[REQUESTING CONTROL]*#*#" in message:
        return True
    else:
        return False

def request_for_answer(question, just_print = False):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect(viki_process.viki_control_transfer_client)

    if just_print:
        question = "*[PRINT]* " + question
    socket.send(question.encode('ascii'),zmq.NOBLOCK)
    response = ""

    if socket.poll(120000, zmq.POLLIN):
        response = socket.recv(zmq.NOBLOCK)
    else:
        pass
    if response == "":
        print("\t\t[ERROR] No response from dqemon")
        return 0
    response = response.decode('ascii')
    print("Question:", question,"\nResponse: ",response)
    socket.close()
    return response

def get_question():
    try_for_lock = True
    while try_for_lock:
        try:
            context = zmq.Context()
            viki_socket = context.socket(zmq.REP)
            viki_socket.setsockopt(zmq.LINGER, 0)
            viki_socket.bind(viki_process.viki_control_transfer_server)
            poller = zmq.Poller()
            poller.register(viki_socket, zmq.POLLIN)
            try_for_lock = False
        except (zmq.error.ZMQError):
            print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
            try_for_lock = True

    def send_answer_for_question(message):
        viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)

    wait_for_question = True
    while wait_for_question:
         # Loop and accept messages from both channels, acting accordingly
        socks = dict(poller.poll(10000))
        if socks:
            if socks.get(viki_socket) == zmq.POLLIN:
                # Get question
                question_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
                question_recieved = str(question_recieved)
                if question_recieved:
                    if question_recieved == "[DONE]":
                        send_answer_for_question("[DONE]")
                        wait_for_question = False
                    else:
                        if question_recieved.startswith("*[PRINT]* "):
                            print("\t ",question_recieved.strip('*[PRINT]* '))
                            send_answer_for_question("..")
                        else:    
                            answer = input("\t "+question_recieved)
                            send_answer_for_question(answer)
        else:
            print ("[INFO] Message timeout")
            wait_for_question = False   