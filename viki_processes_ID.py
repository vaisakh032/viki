
class viki_process:
    networkScannerDemon_client = 'tcp://localhost:2110'
    networkScannerDemon_server = 'tcp://*:2110'

    vikiSpeak_client = 'tcp://localhost:1102'
    vikiSpeak_server = 'tcp://*:1102'

    viki_daemonHandler_client = 'tcp://localhost:1221'
    viki_daemonHandler_server = 'tcp://*:1221'

    viki_eventHandler_client = 'tcp://localhost:5858'
    viki_eventHandler_server = 'tcp://*:5858'

    viki_clock_client = 'tcp://localhost:3885'
    viki_clock_server = 'tcp://*:3885'

    viki_control_transfer_client = 'tcp://localhost:2882'
    viki_control_transfer_server = 'tcp://*:2882'
