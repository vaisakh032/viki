import sys
import time
import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
import threading
from viki_processes_ID import viki_process
import zmq
import os

try:
    context = zmq.Context()
    viki_socket = context.socket(zmq.REP)
    viki_socket.setsockopt(zmq.LINGER, 0)
    viki_socket.bind(viki_process.vikiSpeak_server)
    poller = zmq.Poller()
    poller.register(viki_socket, zmq.POLLIN)
except (zmq.error.ZMQError):
    print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
    exit(0)

process_pid_val = 0
shutdown_flag = threading.Event()
uri_flag = threading.Event()
uri = ""
playMode = False
URILock  = threading.RLock()
playingFile = False

def message_parser(request_recieved):
    global uri
    global process_pid_val
    sender = request_recieved.split('|')[0]
    
    if ":" in request_recieved:
        message = request_recieved.split('|')[1].split(':')[0]
        payload = request_recieved.split('|')[1].split(':')[1]
        print("[Info] Parse Mesasge Filename -> ",payload)
    else:
        message = request_recieved.split('|')[1]
    
    if message.startswith("PlayAudio"):
        print("[REQUEST] Play Audio in location -> ",payload)
        URILock.acquire()
        uri = payload
        print("[INFO] URI FLAG SET!")
        URILock.release()
        uri_flag.set()
        uri_flag.clear()
        send_response_for_request("Playing Audio |P0")
    elif message.startswith("PlayString"):
        speakFromString(payload)
        send_response_for_request("Completed String Playback|P0")         
    elif message == "STATUS":
        send_response_for_request("UP with Process ID->"+str(process_pid_val)) 
    elif message == "REPORT":
        send_response_for_request("Nothing Report")             
    else:
        send_response_for_request("Invalid Request")    

    
def send_response_for_request(message):
    message = "[Viki Speak Demon]@ "+message
    viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)


def check_for_requests():
    global uri
    # Loop and accept messages from both channels, acting accordingly
    socks = dict(poller.poll(4000))
    if socks:
        if socks.get(viki_socket) == zmq.POLLIN:
            # Get request
            request_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
            print(request_recieved)
            if request_recieved:
                if request_recieved.split('|')[1] == "[TERMINATE]":
                    send_response_for_request("Terminating daemon")
                    uri = ""
                    uri_flag.set()
                    shutdown_flag.set()
                    return True
                else:    
                    message_parser(request_recieved)
                    return False
    else:
        print ("[INFO] Message timeout")
        return False



def bus_call(bus, message, ):
    if message.type == Gst.MessageType.EOS:
        print("[ERROR] - EOS ")
        playMode = True

def speakFromString(speak_string):
    global URILock
    global uri_flag
    global uri
    global playingFile
    speak_string = speak_string.strip(" ")
    print("\nSpeak String:", speak_string)
    for fileName in speak_string.split(" "):
        while playingFile:
            continue
        URILock.acquire()
        uri = fileName+".mp3"
        print("File Name:", uri)
        URILock.release()
        uri_flag.set()
        uri_flag.clear()
        playingFile = True

def player():
    global uri_flag
    global URILock
    global uri
    global playingFile
    while not shutdown_flag.is_set():
        print("[INFO] Waiting for uri_flag ")
        uri_flag.wait()
        print("[INFO] URI flag set in player")
        URILock.acquire()
        print("[INFO] URI Lock acquired in player for uri ->",uri,".")
        if shutdown_flag.is_set():
            break
        playingFile = True
        if not uri.endswith(".mp3"):
            print("[ERROR] Incorrect file name -> ", uri)
            continue
        temp_uri = "audio_files/"+uri    
        
        if not os.path.exists(temp_uri):
            temp_uri = "audio_files/personal/"+uri
            if not os.path.exists(temp_uri):
                print("[ERROR] File does not exist -> ", uri)
                file_not_found(uri)
                uri = "audio_files/no_audio.mp3"
                uri = Gst.filename_to_uri(uri)
            else:
                uri = temp_uri
                uri = Gst.filename_to_uri(uri)
                        
        else : 
            uri = temp_uri
            uri = Gst.filename_to_uri(uri)
        
        playbin.set_property('uri', uri)
        print("[Info] Playing file")
        
        if playbin.set_state(Gst.State.PLAYING):
            bus = playbin.get_bus()
            msg = bus.timed_pop_filtered(Gst.CLOCK_TIME_NONE, Gst.MessageType.EOS)
            print("[ERROR] EOS")   
        playbin.set_state(Gst.State.NULL)
        URILock.release()
        playingFile = False
    print("[INFO] Player thread Treminated")   

def file_not_found(uri):
    path = 'data/audio_required.txt'
    print("file not found function!")
    files = open(path,'r+')
    for lines in files.readlines():
        print("line: ", lines)
        if lines.strip("\n") == uri:
            return 0        
    files = open(path,'a+')        
    files.write(uri+"\n")
    files.close

def init_player():
    Gst.init(None)
        
    playbin = Gst.ElementFactory.make("playbin", None)
    if not playbin:
        sys.stderr.write("'playbin' gstreamer plugin missing\n")
        sys.exit(1)
    
    playerThread = threading.Thread(target=player)
    playerThread.start()
    return (playbin, playerThread)


if __name__ == "__main__":
    process_pid_val = os.getpid()
    print("[INFO] Creating Pipeline")
    (playbin, playerThread) = init_player()
    exit_daemon = False
    while not exit_daemon:
        exit_daemon = check_for_requests()
    
    playerThread.join()
    print("[INFO] Player Stoped")
