import nmap
import os
from bluepy.btle import Scanner, DefaultDelegate
from viki_processes_ID import viki_process
import zmq
import json
import datetime
import threading
import time

try:
    context = zmq.Context()
    viki_socket = context.socket(zmq.REP)
    viki_socket.setsockopt(zmq.LINGER, 0)
    viki_socket.bind(viki_process.networkScannerDemon_server)
    poller = zmq.Poller()
    poller.register(viki_socket, zmq.POLLIN)
except (zmq.error.ZMQError):
    print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
    exit(0)

process_pid_val = 0
scanReport = {}
scanReportLock  = threading.RLock()
scanNetowrkInterval = True

bleScanTime = 15
networkScanTime = 330
    
def scan_port(forceScan = False):
    global scanNetowrkInterval
    while scanNetowrkInterval:
        print("scanNetowrkInterval: ", scanNetowrkInterval)

        print("[INFO] Starting IP SCAN")
        nm = nmap.PortScanner()
        nm.scan(hosts='192.168.1.1/24', arguments='-sP')
        host_list = nm.all_hosts()
        print("All hosts:\n",host_list)
        mac_ids = []
        ip_and_mac = {}
        for host in host_list:
            if  'mac' in nm[host]['addresses']:
                print("total details: ", nm[host]['addresses'],"ip: ")
                mac_ids.append(str(nm[host]['addresses']['mac']).lower())
                if 'ipv4' in nm[host]['addresses']:
                    ip_and_mac[str(nm[host]['addresses']['mac']).lower()] = str(nm[host]['addresses']['ipv4']) 

        scanReportLock.acquire()
        scanReport ['network'] = mac_ids
        scanReport ['network_scan_time'] = str(datetime.datetime.now())
        scanReport ['network_mac_ip'] = ip_and_mac  
        scanReportLock.release()
        if forceScan:
            return mac_ids
        if not forceScan:
            i = 0 
            while (i*5 < networkScanTime) and scanNetowrkInterval :
                time.sleep(5)
                i = i + 1            
    


def bluetooth_scan(forceScan = False):
    target_mac = "dc:16:b2:a0:a5:7c"
    class ScanDelegate(DefaultDelegate):
        def __init__(self):
            DefaultDelegate.__init__(self)

        def handleDiscovery(self, dev, isNewDev, isNewData):
            if isNewDev:
                pass
                #print("Discovered device", dev.addr)
            elif isNewData:
                pass
                #print("Received new data from", dev.addr)

    scanner = Scanner().withDelegate(ScanDelegate())
    global scanNetowrkInterval
    while scanNetowrkInterval:
        print("[BLE] Scan : ", scanNetowrkInterval)
        devices = scanner.scan(2.5)
        print("Ble devices: ",devices)
        devicesList = []
        for dev in devices:
            devicesList.append(dev.addr)
            if (str(dev.addr).lower()==target_mac):
                print ("Device Found: %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
                #for (adtype, desc, value) in dev.getScanData():
                #    print ("  %s = %s" % (desc, value))
        scanReportLock.acquire()
        scanReport['BLE'] = devicesList
        scanReport['BLE_scan_time'] = str(datetime.datetime.now())
        scanReportLock.release()
        if forceScan:
            return devicesList
        if not forceScan:
            i = 0
            while (i*5 < bleScanTime) and scanNetowrkInterval :
                time.sleep(5)
                i = i + 1    

def message_parser(request_recieved):
    
    global process_pid_val
    sender = request_recieved.split('|')[0]
    message = request_recieved.split('|')[1]
    if message == "ScanNetworkNow":
        print("[RESPONSE] Force network scan")
        send_response_for_request(str(scan_port(True)))
    elif message == "ScanBleNow":
        print("[RESPONSE] Force BLE scan")
        send_response_for_request(str(bluetooth_scan(True)))     
    elif message == "STATUS":
        send_response_for_request("UP with Process ID->"+str(process_pid_val)) 
    elif message == "REPORT":
        scanReportLock.acquire()
        #reportMessage = str(scanReport)
        print("Scan report type: ", type(scanReport))
        reportMessage = ""
        reportMessage = json.dumps(scanReport)
        print("Direct Report: ",reportMessage)
        scanReportLock.release()
        send_response_for_request(str(reportMessage))        
    else:
        print("Invalid request")
        time.sleep(5)
        send_response_for_request("Invalid Request")    

    
def send_response_for_request(message):
    message = "[Mac Scan Demon]@ "+message
    viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)

def check_for_requests():
    # Loop and accept messages from both channels, acting accordingly
    socks = dict(poller.poll(4000))
    if socks:
        if socks.get(viki_socket) == zmq.POLLIN:
            # Get request
            request_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
            print(request_recieved)
            if request_recieved:
                if request_recieved.split('|')[1] == "[TERMINATE]":
                    send_response_for_request("Terminating daemon")
                    scanNetowrkInterval = False
                    return True
                else:    
                    message_parser(request_recieved)
                    return False
    else:
        print ("[INFO] Message timeout")
        return False


if __name__ == "__main__":
    if os.geteuid() != 0:
        print("[ERROR] Please run as root user!")
        exit()

    process_pid_val = os.getpid()
    print("[CTRL] Started macScanner Daemon")
    end_process = False
    networkScannerThread = threading.Thread(target=scan_port)
    bleScannerThread = threading.Thread(target=bluetooth_scan)
    networkScannerThread.start()
    bleScannerThread.start()
    print("started thread!")
    #scan_port()
    while not end_process:
        end_process = check_for_requests()
        print("waiting netThred:",bleScannerThread.is_alive())
        print("waiting netThred:",networkScannerThread.is_alive())    

    print("[CTRL] Terminating macScanner Daemon")
    scanNetowrkInterval = False
    # bleScannerThread.join()
    # bleScannerThread = threading.Thread(target=bluetooth_scan)
    # bleScannerThread.start()
    # time.sleep(5)
    bleScannerThread.join()
    networkScannerThread.join()
    viki_socket.close()    