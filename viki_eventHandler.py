import zmq
from viki_processes_ID import viki_process
import ast
import json
from datetime import datetime
import os
import time
import requests

main_report = []
people_in_house = []
target_macs = {}
process_pid_val = ""
list_unknown_guests = {}
unknown_macs = []

try:
    context = zmq.Context()
    viki_socket = context.socket(zmq.REP)
    viki_socket.bind(viki_process.viki_eventHandler_server)
    poller = zmq.Poller()
    poller.register(viki_socket, zmq.POLLIN)
except (zmq.error.ZMQError):
    print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
    exit(0)



def request_and_recieve(port,command,force_wait = "False"):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect(port)

    command = "[EVHR]|"+command
    socket.send(command.encode('ascii'),zmq.NOBLOCK)
    response = ""
    if force_wait == "True":
        if socket.poll(60000, zmq.POLLIN):
            response = socket.recv(zmq.NOBLOCK)
        else:
            pass

    else:
        if socket.poll(4000, zmq.POLLIN):
            response = socket.recv(zmq.NOBLOCK)
        else:
            pass

    socket.close()
    if response:
        response = response.decode('ascii')
        sender = (response.split('@')[0])
        message = response.split('@')[1]        
        return (sender,message)
    else:
        return ("","")    



def surround__awarness( filter = "important"):
    global target_macs
    global unknown_macs
    return_string = ""
    def get_target_macs():
        global target_macs
        filename = 'data/target_macs.json'
        with open(filename, 'r') as outfile:
            target_macs = json.load(outfile)
        print("[INFO] Target mac lists updated!")
    
    def update_macReport(data):
        if os.path.exists("data/surround_awarness_report.txt"):
            file  = open("data/surround_awarness_report.txt","a+")
        else:
            file  = open("data/surround_awarness_report.txt","w+")
        
        if not file:
            print("Cannot open files")
        file.write(str(data)+"\n")
        print("[INFO] Report updated!")

    if target_macs == {}:
        get_target_macs()

    ( sender, data ) = request_and_recieve(viki_process.networkScannerDemon_client,"REPORT",force_wait = "True")
    if data == "" :
        return ("no response from daemon","")
    data = json.loads(data)
    if sender != "[Mac Scan Demon]":
        main_report.append("SUA10>Expected Meesage from >Mac Scan Demon >response from >"+str(sender))
        return False
    now = datetime.now()
    print("Now: ", now)
    if 'network' in data.keys():
        print("Network Macs: ", data['network'],"\nTargets: ", target_macs)
        network_list = data['network']
        people_in_house = []

        for keyss in target_macs.keys():
            if target_macs[keyss] in network_list:
                network_list.remove(target_macs[keyss])
                people_in_house.append(keyss)
                print("From Scan: "+str(keyss))
        print("Unknown macs: ", unknown_macs)

        #lines to create a string for viki_speak
        speak_string = "we_have "
        less_prior_devices = 0
        total_import_people = 0
        list_of_names = [] 
        for persons in people_in_house:
            if "|P0" not in persons:
                total_import_people = total_import_people + 1
                list_of_names.append(persons)
            else:
                less_prior_devices = less_prior_devices + 1
        
        i = 0        
        for names in list_of_names:
            i = i + 1
            speak_string = speak_string + "personal/"+str(names) + " "
            if (i == total_import_people-1) and total_import_people > 1 and len(network_list) == 0 and filter != "all" :
                speak_string = speak_string + "and "

        print("Total important people: ",total_import_people," less prior device: ",less_prior_devices,"unknown device: ", network_list)
        if len(network_list) > 0:
            if " and " not in speak_string and (less_prior_devices != 0 and filter != "all"):
                speak_string = speak_string + "and "
            print("Less prior = ",less_prior_devices)
            if len(network_list) == 1:    
                speak_string = speak_string + "1 guest "
            elif len(network_list) > 1:    
                speak_string = speak_string + str(len(network_list))     + " guests "    
        
        if less_prior_devices > 0 and filter == "all":
            if " and " not in speak_string and (total_import_people > 0 or len(network_list)>0):
                speak_string = speak_string + "and "
            if less_prior_devices == 1:    
                speak_string = speak_string +str(less_prior_devices) + " device "
            else:
                speak_string = speak_string +str(less_prior_devices) + " devices "

        elif (len(people_in_house)+ len(network_list) - less_prior_devices) < 1:
            speak_string = speak_string + "no_one"                
        speak_string = speak_string.rstrip(" ") + " in_the_network"
        
        if network_list != []:        
            print("Those without saved name: ",network_list)
            unknown_macs.extend(network_list)
        else:
            print("Only known persons in home network")
        total_macs_in_house = people_in_house + network_list
        
        return_string = return_string + "\n\t People in House: "+str(len(people_in_house))+"\n\t\t"
        print("return_string: ", return_string)
        tmp_list = []
        for people in people_in_house:
            if "|P0" in people:
                tmp_list.append(people)
            else:     
                return_string = return_string + str(people) + ", "
        return_string = return_string.rstrip(", ")
        if filter == "all":
            return_string = return_string +"\n\t"+"-"*16+"\n\t Devices in House: "+str(len(tmp_list))+"\n\t\t"
            for devices in tmp_list:
                return_string = return_string + str(devices) + ", "
            return_string = return_string.rstrip(", ")             
        print("return_string: ", return_string)
        return_string = return_string +"\n\t"+"-"*16+"\n\t Guests in House: "+str(len(network_list)) +"\n\t\t"
        for guests in network_list:
            return_string = return_string +str(guests) + ", "
        return_string = return_string.rstrip(", ")            
        
        print("return_string: ", return_string)
        update_macReport({now:total_macs_in_house})
        return (return_string,speak_string)
    else:
        #speakFromString("scanner_daemon_isn't_ready_yet")
        return ("[ERROR] scanner daemon not ready","scanner_daemon_isn't_ready_yet")                 

def fetch_details_of_guests():
    now = datetime.now()
    global list_unknown_guests
    global unknown_macs
    if ( len(unknown_macs) == 0 ):
        speakFromString("we_dont_have_any_unkown_guests_in_the_network")
        return "we dont have any any unkown guests"

    speakFromString("we_have " + str(len(unknown_macs)) + " guests" + " in_the_network")
    print("unknown_macs:" , unknown_macs)
    speakFromString("fetching_mac_details_of_guests_from_list")
    i = 1
    internetOut = False
    return_string = "Unknown Guests: " + str(len(unknown_macs))
    return_string = return_string + "\n\t-----------------------------------------------"
    for macs in unknown_macs:
        speakFromString("guest "+str(i))
        return_string = return_string + "\n\t  Guest:" + str(i)
        return_string = return_string + "\n\t\t Mac:" + str(macs)
        try:
            details = lookup_mac_api_connect(macs)
        except ConnectionError:
            internetOut = True
        if not details:
            return_string = return_string + "\n\t\t Error : Couldn't fetch details. Internet connection not found."
            return "Internet connection not found"

        people_in_house.append(macs)
        if 'company' in details['result'].keys():
            list_unknown_guests[macs] = [now , details['result']['company']]
            company = (str(details['result']['company']).replace(" ","_")).lower()
            spl_chrs = [",", ".", "/", "-", ":"]
            for chrs in spl_chrs:
                if chrs in company:
                    company = company.replace(chrs,"")
            speakFromString("mac seems to belong to "+company)
            print(macs," seems to belong to ",details['result']['company'] )
            return_string = return_string + "\n\t\t Company: " + str(details['result']['company'])
            return_string = return_string + "\n\t\t TimeStamp: " + str(now.date()) + ", " + str(now.time().strftime("%H:%M"))
            return_string = return_string + "\n\t-----------------------------------------------"
        else:
            return_string = return_string + "\n\t\t ERROR: Couldn't Fetch Details.\n ->"+ str(details)
            list_unknown_guests[macs] = [now , details]
        #unknown_macs.remove(macs)
        print("unknown_macs:" , unknown_macs)
        i = i+1
    return return_string    


def speakFromString(speak_string):
    (sender, response) = request_and_recieve(viki_process.vikiSpeak_client, "PlayString:"+speak_string, "False")
    print( "Response:\n\tSender  : ", sender,"\n\tResponse: ",response)


def lookup_mac_api_connect(mac_addr):
    MAC_URL = 'http://macvendors.co/api/'+str(mac_addr)
    try:
        r = requests.get(MAC_URL)
    except requests.ConnectionError:
        speakFromString("please_check_the_internet_connection")
        return {}
    return r.json()


def send_response_for_request(message):
    message = "[Event Handler]@ "+message
    viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)


def message_parser(request_recieved):
    global daemon_list
    global process_pid_val
    
    sender = request_recieved.split('|')[0]
    message = request_recieved.split('|')[1]
    
    if message == "STATUS":
        send_response_for_request("UP with Process ID->"+str(process_pid_val))                  
    elif message == "PEOPLE_IN_NETWORK" or message == "ALL_IN_NETWORK":
        if message == "ALL_IN_NETWORK":
            (return_string,speak_string) = surround__awarness("all")
        else:
            (return_string,speak_string) = surround__awarness()
        print_string = speak_string
        print_string = print_string.replace("personal/","")
        print_string = print_string.replace("_"," ")
        send_response_for_request(return_string+"\n\t"+"-"*46+"\n\t Speaking ->"+str(print_string)+"\n\t"+"-"*46)
        if speak_string != "":
            speakFromString(speak_string)
    elif message == "FETCH_MAC_DETAILS":
        send_response_for_request(str(fetch_details_of_guests()))
    else:
        send_response_for_request("Invalid Request")    


def check_for_requests():
    # Loop and accept messages from both channels, acting accordingly
    socks = dict(poller.poll(4000))
    if socks:
        if socks.get(viki_socket) == zmq.POLLIN:
            # Get request
            request_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
            print(request_recieved)
            if request_recieved:
                if request_recieved.split('|')[1] == "[TERMINATE]":
                    send_response_for_request("Terminating daemon")
                    return True
                else:    
                    message_parser(request_recieved)
                    return False
    else:
        print ("[INFO] Message timeout")
        return False


if __name__ == "__main__":
    process_pid_val = os.getpid()
    #surround__awarness("all")
    exit_daemon = False
    print("[INFO] Event Handler Started")
    while not exit_daemon:
        exit_daemon = check_for_requests()
    
    print("[INFO] Event Handler Terminated")
    exit(1)