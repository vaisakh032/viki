from viki_processes_ID import viki_process
import zmq
import sys
import os
from tabCompletion import Tabcomplete
import json
import time
import viki_controlTransfer as vikiCtl

command_translation_table = {}
daemon_pids = ""
class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[41m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ITALICS = '\033[3m'
    UNDERLINE = '\033[4m'
    BGYellow = '\033[43m'
    YELLOW = '\033[0;33m'
    END = '\033[0m'
    BLINK = '\033[5m'
    GREY = '\033[90m'

def display_acii_image():
    f = open("essentials/viki_asci.txt", "r")
    strin = ""
    for lines in f.readlines():
        strin = strin + str(lines)
    print("\033[34m"+strin+"\033[0m")

def load_translation():
    global command_translation_table
    filename = 'essentials/command_translation.json'
    if os.path.exists(filename):
        try:    
            with open(filename) as json_data:
                command_translation_table = json.load(json_data)
                print("\t[....] Command Translation Table initialised")  
                return True
        except:
            pass
    print("  [ERROR]"+colors.FAIL+" Couldn't initialise Command translator!"+colors.GREY+"[File not found]"+colors.END)
    return False    

def update_translation():
    global command_translation_table
    filename = 'essentials/command_translation.json'
    with open(filename, 'w') as outfile:
        json.dump(command_translation_table, outfile)
    print("[INFO] Translation table updated!")

def request_and_recieve(port,raw_command,force_wait = "False"):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect(port)

    command = "[viki]|"+raw_command
    socket.send(command.encode('ascii'),zmq.NOBLOCK)
    response = ""

    if force_wait == "True":
        if socket.poll(60000, zmq.POLLIN):
            response = socket.recv(zmq.NOBLOCK)
        else:
            pass

    else:
        if socket.poll(4000, zmq.POLLIN):
            response = socket.recv(zmq.NOBLOCK)
        else:
            pass
    if response == "":
        print("\t\t[ERROR]"+colors.FAIL+" No response from daemon -> "+str(port)+colors.END)
        if port != viki_process.viki_daemonHandler_client:
            print("\t\t[REQ]"+colors.YELLOW+" Sending Request to handle daemon with port -> "+colors.END+str(port))
            request_and_recieve(viki_process.viki_daemonHandler_client,"HANDLE_PORT->"+str(port))
            ip = input("\tRetry Request (y/n)?: ")
            if ip.lower() == 'y' or ip.lower() ==" yes":
                request_and_recieve(port,raw_command,force_wait)
            return 0
        else:
            print(colors.BLINK+colors.FAIL+"\t\t[EXCEPTION] No response from Daemon Hanlder "+colors.END)        
            return 0 
    response = response.decode('ascii')
    priority = 1
    if response.endswith("|P0"):
        response = response.split("|P0")[0]
        priority = 0
    message = ""

    sender = str(response.split('@')[0])
    if len(response.split('@')) == 2:
        message = str(response.split('@')[1])
    else:
        message = str(response.split('@')[1:])
            
    if vikiCtl.check__if_control_requested(str(message).strip(' ')):
        print(colors.GREY+colors.ITALICS+"Control Requested"+colors.END)
        vikiCtl.get_question()
    else:
        if priority != 0:    
            print(colors.OKBLUE+"  Response:\n\t"+colors.YELLOW+"Sender  :"+colors.END,sender)
            print("\t"+colors.YELLOW+"Message :"+colors.END+str(message))        
    
    socket.close()
    return message     

def command_parser(user_command):
    global command_translation_table
    for commands in command_translation_table:
        if user_command == commands:
            request_and_recieve(eval(command_translation_table[user_command][0]),command_translation_table[user_command][1],command_translation_table[user_command][2])
            return 0 
        elif "*" in commands and user_command.startswith((str(str(commands).split('*')[0])).rstrip(' ')):
            payload = user_command
            payload = payload.replace((str(str(commands).split('*')[0])).rstrip(' ') ,"")
            payload = payload.strip(' ')
            if payload == '':
                print("  [ERROR]"+colors.FAIL+" Incomplete command!"+colors.END)            
            else:
                request_and_recieve(eval(command_translation_table[commands][0]),str(command_translation_table[commands][1])+payload,command_translation_table[commands][2])
            return 0
    
    print("  [ERROR]"+colors.FAIL+" Command not found!"+colors.END)


def add_new_command(new_command):
    global command_translation_table
    if new_command in command_translation_table:
        print(colors.YELLOW+"[WARNING]"+colors.END+"Command exists in database!, be carefull")
    print(colors.GREEN+"New Command: "+colors.END,new_command)
    choice = 'n'
    portAddr = ""
    while choice == 'n':
        portAddr = "viki_process."+str(input(" Enter port address in terms of viki_process_id: \n\t"))
        try:
            print(colors.OKBLUE+"\t Address:"+colors.END,eval(portAddr))
            choice = (str(input("  CONFIRM"+colors.GREEN+" (y/n) :"+colors.END))).lower()
            if choice !='y': choice = 'n'
        except (NameError,AttributeError):
            print("  [ERORR] "+colors.FAIL+"Port "+portAddr+" not found  Please retry!"+colors.END)
            choice = 'n'
    message = str(input("  Enter command to be send to process: "))
    netowrkWait = (str(input("  Do I have to wait for reply "+colors.GREEN+"(y/n): "+colors.END))).lower()
    if netowrkWait == 'y' : netowrkWait = "True"
    else : netowrkWait = "False"

    command_translation_table[new_command]=[portAddr,message,netowrkWait]        
    print(command_translation_table)
    update_translation()
    return True

def start_daemon_handler(initialise_with_string = False):
    global daemon_pids
    os.system('(sudo python3 viki_daemonHandler.py) >>/dev/null &')
    request_and_recieve(viki_process.viki_daemonHandler_client, "STATUS", "True")
    print(colors.GREY+"\t       .....please wait"+colors.END)
    #time.sleep(10)
    if initialise_with_string:
        print("\t[....] Re-initialising Daemons with pids.")
        print(colors.GREY+"\t       .....waiting for response from daemon handler"+colors.END)
        request_and_recieve(viki_process.viki_daemonHandler_client, "INITIAL->"+str(daemon_pids), "True")
    else:
        print("\t[....] Starting ALL registered Daemons.")
        print(colors.GREY+"\t       .....waiting for response from daemon handler"+colors.END)    
        daemon_pids = request_and_recieve(viki_process.viki_daemonHandler_client, "START->all", "True")
    request_and_recieve(viki_process.viki_daemonHandler_client, "REPORT", "True")

if __name__ == "__main__":
    #clear terminal screen
    os.system('clear')
    display_acii_image()
    print("[INFO]"+colors.OKBLUE+colors.BOLD+" Initialising Viki"+colors.END)
    print("\t[....] Invoking Daemon Handler")
    start_daemon_handler()
    load_translation()
    viki_auto_complete = Tabcomplete("essentials/viki_commands.json")
    debug_mode = False

    # just for simplicity of addition of commands
    if len(sys.argv)>1:
        if sys.argv[1] == "-a":
            debug_mode = True
            print("\t[....]"+colors.BGYellow+" Starting in debug mode\n"+colors.END, sys.argv[1])
    # till here

    request_exit = False
    print("\t[....] Viki_speak Boot time Greeting.")
    request_and_recieve(viki_process.vikiSpeak_client, "PlayAudio:onboot.mp3", "False")
    print(colors.OKBLUE+colors.BOLD+"[INFO] Initialisation Complete."+colors.END)
    print(colors.GREY+colors.BOLD+"[VIKI] Dad.. The console is all yours."+colors.END)
    while not request_exit:
        try:
            if debug_mode:
                print(colors.YELLOW+"Anything you type will be added as a new command, be carefull"+colors.END)
                command = viki_auto_complete.getip(colors.FAIL+"[debug_mode]"+colors.GREEN+" Enter command>> "+colors.END, "basic commands")
                if command == "exit":
                    request_exit = True
                else:
                    add_new_command(command)
                    continue
            else:
                command = viki_auto_complete.getip(colors.GREEN+"Enter command>> "+colors.END, "basic commands", dynamicUpdation='n')
            if command == "exit":
                request_exit = True
            elif command == "start daemon_handler":
                start_daemon_handler(initialise_with_string = True)
            else:
                command_parser(command)    
        except (KeyboardInterrupt, SystemExit):
            print(colors.GREY+"\nPlease enter exit to close viki"+colors.END)

    request_and_recieve(viki_process.vikiSpeak_client, "PlayAudio:onshutdown.mp3", "False")
    request_and_recieve(viki_process.viki_daemonHandler_client, "STOP->all", "True")
    request_and_recieve(viki_process.viki_daemonHandler_client, "[TERMINATE]", "True")            
    print("[INFO] Ending vikin main thread")