from viki_processes_ID import viki_process
import sys
import os
import zmq
import time
import json
import viki_controlTransfer as vikiCtl

try:
    context = zmq.Context()
    viki_socket = context.socket(zmq.REP)
    viki_socket.bind(viki_process.viki_daemonHandler_server)
    poller = zmq.Poller()
    poller.register(viki_socket, zmq.POLLIN)
except (zmq.error.ZMQError):
    print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
    exit(0)


daemon_list = {}
daemon_pid_list = {}
daemon_register = {}
process_pid_val = ""

def request_and_recieve(port, job ,force_wait = False):
    daemon_context = zmq.Context()
    daemon_socket = context.socket(zmq.REQ)
    daemon_socket.setsockopt(zmq.LINGER, 0)
    daemon_socket.connect(port)

    if job == "start":
        command = "[Daemon Starter]|STATUS"
    elif job == "stop":
        command = "[Daemon Starter]|[TERMINATE]"    
    daemon_socket.send(command.encode('ascii'),zmq.NOBLOCK)
    if force_wait == "True":
        if daemon_socket.poll(30000, zmq.POLLIN):
            response = daemon_socket.recv(zmq.NOBLOCK)
            response = response.decode('ascii')
        else:
            print("[INFO] No Response from demon")
            response = "no response"

    else:
        print("Short Timmer!")
        if daemon_socket.poll(4000, zmq.POLLIN):
            response = daemon_socket.recv(zmq.NOBLOCK)
            response = response.decode('ascii')
        else:
            print("[INFO] No Response from demon")
            response = "no response"

    daemon_socket.close()
    return response        

def daemon_starter(daemon, silent = False ):
    daemonFound = False
    for registered_daemon in daemon_register.keys():
        daemon_details = daemon_register[registered_daemon]
        if daemon == "all" or daemon == daemon_details['condition_tag']:
            daemonFound = True
            print("[INFO] Satrting ",registered_daemon)
            os.system(daemon_details['command'])
            time.sleep(1)
            response = request_and_recieve(eval(daemon_details['addr']) , "start" )
            if response == "no response" or response == None:
                print("[ERROR] couldn't start "+str(registered_daemon)+" .... (exiting daemon handler)")
                daemon_list[registered_daemon] = "ERROR"
                daemon_pid_list[registered_daemon] = {"Daemon_Name":0,"port":0, "pid":0}
                #exit(0)
            elif response.startswith(daemon_details['start_response_string']):
                daemon_list[registered_daemon] = response.split('>')[1]
                daemon_pid_list[registered_daemon] = {"Daemon_Name":daemon_details['condition_tag'],"port":eval(daemon_details['addr']), "pid":response.split('>')[1]}
            else:
                print("[ERROR] Unparsable Response"+response)
                daemon_list[registered_daemon] = "[ERROR] Unparsable Response"+response
                daemon_pid_list[registered_daemon] = {"Daemon_Name":0,"port":0, "pid":0}

            if daemon != "all" and not silent:
                send_response_for_request("Daemon Started with PID: "+str(daemon_pid_list[registered_daemon]['pid']))
                return 0
            if daemon != "all" and silent:
                return 0                 
        else:
            continue

    if not daemonFound:
        print("\t [....] Couldn't find daemon. Daemon data not found in list.")
        send_response_for_request("Couldn't find daemon. Daemon data not found in list.")    
    if daemon == "all":
        send_response_for_request(str({"daemon_list":daemon_list, "daemon_pid_list":daemon_pid_list})+"|P0")            
    
    return 0                

def daemon_killer(daemon, silent = False ):
    global daemon_list
    global daemon_pid_list
    for registered_daemon in daemon_register.keys():
        daemon_details = daemon_register[registered_daemon]
        if daemon == "all" or daemon == daemon_details['condition_tag']:
            print("[INFO] Stoping ",registered_daemon)
            if daemon_pid_list != {} and registered_daemon in daemon_pid_list.keys()  and daemon_pid_list[registered_daemon]['pid'] == 0:
                print("\t [....] Daemon Already Killed!")
                if daemon != "all":    
                    send_response_for_request("Daemon not spawned or killed mannually.")
                    return 0
                else:
                    print("\t [....] continue loop...")
                    continue
            if registered_daemon not in daemon_pid_list.keys():
                daemon_pid_list[registered_daemon] = {}    
            response = request_and_recieve(eval(daemon_details['addr']), "stop" , force_wait = True )
            if response == None:
                print("[ERROR] couldn't Stop "+str(registered_daemon)+" .... (kill manually)")
                daemon_list[registered_daemon] = "Alive"
                if daemon_forceKill(registered_daemon):
                    daemon_list[registered_daemon] = "ForceKilled"
                daemon_pid_list[registered_daemon]['pid'] = 0
            elif response.startswith(daemon_details['stop_response_string']):
                daemon_list[registered_daemon] = "Killed"
                daemon_pid_list[registered_daemon]['pid'] = 0
            else:
                print("[ERROR] Unparsable Response"+response)
                daemon_list[registered_daemon] = "ERROR"
                if daemon_forceKill(registered_daemon):
                    daemon_list[registered_daemon] = "ForceKilled"
                daemon_pid_list[registered_daemon]['pid'] = 0    

            if daemon != "all" and not silent:
                send_response_for_request("Daemon Stoped with PID: "+str(daemon_pid_list[registered_daemon]['pid']))
                return 0    
            if daemon != "all" and silent:
                return 0        

        else:
            continue
    if daemon == "all":            
        outputString = "Total Number of daemon Alive : " + str(len(daemon_list.keys()))
        outputString = outputString + "\n\t\t\t Daemon Name"+ " "*15+" PID Value"
        for k in daemon_list.keys():
            outputString = outputString+"\n\t\t" + str(k) + "."*(35-len(str(k))) +": "+ str(daemon_list[k])
        send_response_for_request(outputString)

def daemon_forceKill(daemon_name):
    if daemon_name != "" and daemon_name in daemon_pid_list:
        print("[INFO] Force Kill ",daemon_name," PID: ",str(daemon_pid_list[daemon_name]['pid']))
        if str(daemon_pid_list[daemon_name]['pid']) != "0":
            os.system("sudo kill "+str(daemon_pid_list[daemon_name]['pid']))
        else:
            print("\t [ERROR] Exception handled. Requested Force kill for pid val: 0")
        return True
    else:
        return False

def search_for_daemon(registered_daemon_name ="", port = "", daemon_name = ""):
    daemon_details = {}

    print("[INFO] Request to Handle Daemon with Registered Name: ", registered_daemon_name, "or Daemon Name: ", daemon_name ," or with portAddr: ", port)
    print("\t [....] searching for daemon in resitered_daemon")
    for active_daemons in daemon_pid_list.keys():
        daemon_details = daemon_pid_list[active_daemons]
        if registered_daemon_name != "" and registered_daemon_name == active_daemons:
            
            registered_daemon_name = active_daemons
            break
        elif port != "" and port == daemon_details['port']:
            registered_daemon_name = active_daemons
            break
        elif daemon_name != "" and daemon_name == daemon_details['Daemon_Name']:
            registered_daemon_name = active_daemons
            break
        else:
            daemon_details = {}
            registered_daemon_name = ""

    if daemon_details and registered_daemon_name != "":
        print("\t [....] Found PID = ", daemon_details['pid'])
        return (registered_daemon_name , daemon_details)
    else:
        print("\t [....] Daemon not found")
        return ("" , "")

def handle_daemon(daemon_name ="", port = ""):

    (daemon_name , daemon_details) = search_for_daemon(registered_daemon_name = daemon_name, port = port)
    if not daemon_details:
        return 0

    checkAlive = True
    print("[INFO] Checking If daemon is active with old PID")
    while checkAlive:
        if int(daemon_details['pid']) != 0:
            try:
                #not killing the daemon, checking if its alive.
                os.kill(int(daemon_details['pid']), 0)
            except OSError:
                checkAlive = False
            else:
                checkAlive = True
        else:
            print("\t\t[....] Daemon not spawned or succesfuly killed.")
            checkAlive = False        

        if checkAlive:
            print("\t[....] Daemon Alive. killing Daemon")
            daemon_killer(daemon_details['Daemon_Name'], silent = True)
        else:
            print("\t[....] Daemon Not alive")

    print("\t[....] Restarting Daemon Name:", daemon_details['Daemon_Name']," old PID: ",daemon_details['pid'])
    daemon_starter(daemon_details['Daemon_Name'], silent = True)
    print("[....] Restarted ",daemon_details['Daemon_Name']," with new PID: ",daemon_pid_list[daemon_name]['pid'])


def send_response_for_request(message):
    message = "[Daemon Handler]@ "+message
    viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)


def message_parser(request_recieved):
    global daemon_list
    global process_pid_val
    sender = request_recieved.split('|')[0]
    
    message = request_recieved.split('|')[1]
    
    if message == "STATUS":
        send_response_for_request("UP with Process ID->"+str(process_pid_val))

    elif message.startswith("INITIAL->"):
        initial_string = str(message.split('>')[1]) 
        print("[Message] = ", message)   
        print("[INFO] Initilise with string -> ",initial_string)
        send_response_for_request("Initialising pid_list from string")
        pid_dict = eval(initial_string)
        daemon_list = pid_dict["daemon_list"]
        daemon_pid_list = pid_dict["daemon_pid_list"]

    elif message.startswith("START->"):
        daemon_detail = str(message.split('>')[1])
        if daemon_detail == "all":
            daemon_starter("all")
        else:
            daemon_starter(daemon_detail)    

    elif message.startswith("STOP->"):
        daemon_detail = str(message.split('>')[1]).strip()
        registered_name = ""
        if daemon_detail == "all":
            daemon_killer("all")
            return 0

        elif daemon_detail.startswith("tcp://"):
            (registered_name, full_details ) = search_for_daemon(port = daemon_detail)
        else:
            (registered_name, full_details) = search_for_daemon(registered_daemon_name = daemon_detail,daemon_name = daemon_detail)
        
        if registered_name != "" and full_details != {}:
            daemon_killer(str(full_details['Daemon_Name']))
        else:
            send_response_for_request("Couldn't Find details of requested Daemon")
        return 0 

    elif message == "REPORT":
        outputString = "Total Number of daemon Alive : " + str(len(daemon_list.keys())) + "/" + str(len(daemon_register))
        outputString = outputString + "\n\t\t\t Daemon Name"+ " "*15+" PID Value"
        for k in daemon_list.keys():
            outputString = outputString+"\n\t\t" + str(k) + "."*(35-len(str(k))) +": "+ str(daemon_list[k])
        send_response_for_request(outputString)
    
    elif message == "ADD_NEW":
        add_new_daemon()    
    
    elif message.startswith("HANDLE_PORT->"):
        (registered_name, full_details ) = search_for_daemon(port=message.split('>')[1])
        if registered_name != "" and full_details != {} and int(full_details['pid']) != 0:
            return_string = "Handling "+str(full_details['Daemon_Name'])+" with pid ->"+str(full_details['pid'])    
        elif registered_name != "" and full_details != {} and int(full_details['pid']) == 0:
            return_string = str(full_details['Daemon_Name'])+" was not spanned or was killed manually . Restarting"
        else:
            return_string = "[WARNING] Daemon with port ->"+str(message.split('>')[1])+" not spawned by damon_handler"
        send_response_for_request(return_string)    
        handle_daemon(port=message.split('>')[1])
    else:
        send_response_for_request("Invalid Request")    


def fetch_daemon_register():

    global daemon_register    
    filename = "essentials/daemon_register.json"
    if os.path.exists(filename):
        try:    
            with open(filename) as json_data:
                daemon_register = json.load(json_data)
                print("[Info] Daemon Register initialised", daemon_register)  
                return True
        except:
            print("Cant load json data")
    print("  [ERROR] Couldn't initialise Daemon Register! [File not found]")
    return False

def update_daemon_register( daemon_name, daemon_data):
    global daemon_register
    if daemon_data == "" and daemon_name == "":
        print("[ERROR] Empty data")
        return False
    daemon_register[daemon_name] = daemon_data    
    filename = "essentials/daemon_register.json"
    with open(filename, 'w') as outfile:
        json.dump(daemon_register, outfile)
    print("[INFO] Daemon Register updated!")


def add_new_daemon():
    global daemon_register

    vikiCtl.request_control(send_response_for_request)
    retry = True
    while retry:
        fileName = vikiCtl.request_for_answer("Enter File Name without Extension: ")
        if fileName in daemon_register.keys():
            vikiCtl.request_for_answer("[ERROR] Daemon with same filename already registered.\nPlease retry.", just_print = True) 
            retry = True
        else:
            retry = False    
    daemon_name = vikiCtl.request_for_answer("Enter Daemon Name: ")
    command = vikiCtl.request_for_answer("Enter Command to start daemon: ")
    choice = 'n'
    portAddr = ""
    while choice == 'n':
        portAddr = "viki_process."+str(vikiCtl.request_for_answer("Enter Port addr: viki_process."))
        try:
            vikiCtl.request_for_answer("\t Address:"+str(eval(portAddr)), just_print = True)
            choice = (str(vikiCtl.request_for_answer("  CONFIRM (y/n) :"))).lower()
            if choice !='y': choice = 'n'
        except (NameError,AttributeError):
            vikiCtl.request_for_answer("  [ERORR] Port "+portAddr+" not found  Please retry!", just_print = True)
            choice = 'n'
    start_response_string = vikiCtl.request_for_answer("Enter the response for STATUS command: ")
    stop_response_string = vikiCtl.request_for_answer("Enter the response for TERMINATE command: ")
    update_daemon_register(fileName, {'condition_tag':daemon_name, 'command':command, 'addr':portAddr, 'start_response_string':start_response_string, 'stop_response_string':stop_response_string})
    vikiCtl.request_for_answer("Updated Daemon Register", just_print= True)
    vikiCtl.request_for_answer("[DONE]")



def check_for_requests():
    # Loop and accept messages from both channels, acting accordingly
    socks = dict(poller.poll(4000))
    if socks:
        if socks.get(viki_socket) == zmq.POLLIN:
            # Get request
            request_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
            print(request_recieved)
            if request_recieved:
                if request_recieved.split('|')[1] == "[TERMINATE]":
                    send_response_for_request("Terminating daemon")
                    return True
                else:    
                    message_parser(request_recieved)
                    return False
    else:
        print ("[INFO] Message timeout")
        return False


if __name__ == "__main__":
    process_pid_val = os.getpid()
    fetch_daemon_register()
    if os.geteuid() != 0:
        print("[ERROR] Please run as root user!")
        exit()
    exit_daemon = False
    print("[INFO] Damemon Handler Started")
    while not exit_daemon:
        exit_daemon = check_for_requests()
    
    print("[INFO] Daemon Handler Terminated")
    exit(1)