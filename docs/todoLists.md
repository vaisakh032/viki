Features to be implemented
===============================

## Daemon Handler
- [ ] Implement Auto-start Enable/Disable feature to control each daemon

## CLI Interface
- [ ] Auto-detect remote viki control (over ssh etc).
- [ ] Find a method to enable remote viki control over ssh
- [ ] Implement history of commands with arrow up/down feature.

## VIKI Engine
> The most import feature to be implemented
- [ ] To update all varaible in its correspondng cycles
- [ ] Implement full awarness using the implemented modules
- [ ] The CLI interface should be able to log into the viki engine