import time
import zmq
from viki_processes_ID import viki_process
import datetime
import os
import threading
import viki_controlTransfer as vikiCtl
import json

try:
    context = zmq.Context()
    viki_socket = context.socket(zmq.REP)
    viki_socket.setsockopt(zmq.LINGER, 0)
    viki_socket.bind(viki_process.viki_clock_server)
    poller = zmq.Poller()
    poller.register(viki_socket, zmq.POLLIN)
except (zmq.error.ZMQError):
    print("[ERROR] Cannot start viki_speak daemon. Adress already in use")
    exit(0)

process_pid_val = ""
keepTime = True
dtNow = ""
alarms = {}


def timeKeeper():
    global dtNow

    dtNow = datetime.datetime.now()
    print("Date: ", dtNow.date()," Time: ",dtNow.time().strftime("%H:%M"))
    while keepTime:
        print("Waiting")
        while ( (int(dtNow.time().strftime("%M")) - int(datetime.datetime.now().time().strftime("%M")))==0 and keepTime):
            time.sleep(5)
        dtNow = datetime.datetime.now()
        check_for_alarms()


def load_alarams():
    global alarms
    filename = 'data/alrams.json'
    if os.path.exists(filename):
        try:    
            with open(filename) as json_data:
                alarms = json.load(json_data)
                print("\t[....] Alarms Loaded")  
                return True
        except:
            pass
    print("  [ERROR] Couldn't load Alarms! [File not found]")
    return False    
        
def update_alarms(mode , data):
    global alarms

    if mode not in alarms.keys():
        alarms[mode] = []
    
    alarms[mode].append(data)    
    filename = 'data/alrams.json'
    with open(filename, 'w') as outfile:
        json.dump(alarms, outfile)
    print("[INFO] Translation table updated!")

def set_alarm():
    global alarms
    vikiCtl.request_control(send_response_for_request)
    
    
    #input date
    valid_input = False
    while not valid_input:
        in_date = vikiCtl.request_for_answer("Enter date (DD/MM/YY): ")
        if in_date == ".":
            in_date = str(datetime.datetime.now().date().strftime("%d/%m/%Y"))
            print("Date entered: ", in_date)
        if "/" in in_date:
            day,month,year = in_date.split('/')
        elif "." in in_date:
            day,month,year = in_date.split('.')
        else:
            print("Value Error please retry.")
            vikiCtl.request_for_answer("Invalid Input!", just_print = True)
            continue        
        try:
            datetime.datetime(int(year), int(month),int(day))
            valid_input = True
            in_date = year+"-"+month+"-"+day
        except ValueError:
            print("Value Error please retry.")
            vikiCtl.request_for_answer("Value Error please retry.", just_print = True)
            valid_input = False    
    print("Date: ", in_date)
    vikiCtl.request_for_answer("Date entered: "+in_date, just_print = True)
    
    #input time
    valid_input = False
    while not valid_input:
        in_time = vikiCtl.request_for_answer("Enter Time (HH:MM:A/P): ")
        if in_time.startswith("."):
            if "+" not in in_time:
                in_time = str(datetime.datetime.now().time().strftime("%H:%M"))
            elif "+" in in_time:
                addTime = in_time.split("+")[1]
                if ":" in addTime or "." in addTime:
                    if ":" in addTime:
                        addTimeHour, addTimeMinute = addTime.split(":")
                    elif "." in addTime:
                        addTimeHour, addTimeMinute = addTime.split(".")    
                    print("AddTimeMinute = ", addTimeMinute)
                    in_time = (datetime.datetime.now() + datetime.timedelta(hours=int(addTimeHour),minutes=int(addTimeMinute))).strftime("%H:%M")
                else:
                    addTimeMinute = addTime
                    in_time = (datetime.datetime.now() + datetime.timedelta(minutes=int(addTimeMinute))).strftime("%H:%M")    
            print("Time entered: ", in_time)
            valid_input = True
        elif ":" in in_time or "." in in_time:
            if ":" in in_time:
                hour,minute,AmPm = in_time.split(':')
            elif "." in in_time:
                hour,minute,AmPm = in_time.split('.')
            print("Hour: ", hour, " minute: ", minute, ":", AmPm)
            if int(hour) in range(1,13) and int(minute) in range (0,60) and AmPm.lower() in ["a","p"]:
                if AmPm.lower() == "p":
                    hour = int(hour) + 12
                valid_input = True
            else:
                print("Invalid Input")
                vikiCtl.request_for_answer("Value Error please retry.", just_print = True)
                valid_input = False
            in_time = str(hour)+":"+str(minute)           
    print("Time: ", in_time)
    vikiCtl.request_for_answer("Time entered: "+in_time, just_print = True)
    
    valid_input = False
    in_mode = ""
    while not valid_input:
        in_mode = vikiCtl.request_for_answer("Repeat ->0.No Repeat 1.Daily, 2.Weekly , 3.Monthly , 4.Yearly: ")
        
        if int(in_mode) in range(0,4):
            valid_input = True
        else:
            vikiCtl.request_for_answer("Not a valid input...Please Retry.", just_print = True)    

    update_alarms(in_mode , {'date':in_date , 'time':in_time})
    vikiCtl.request_for_answer("Done -> "+str(alarms), just_print = True)    
    vikiCtl.request_for_answer("[DONE]")
    return 0 
    
def check_for_alarms():
    global dtNow
    global alarms

    print("Checking Alarm, daile: ")
    print("date: ", dtNow.date()," time: ",dtNow.time().strftime("%H:%M"))
    timeNow = dtNow.time().strftime("%H:%M")
    dateNow = str(dtNow.date())
    
    #No Repeats
    if "0" in alarms.keys():
        for alarm_data in alarms["0"]:
            if timeNow == alarm_data["time"] and dateNow == alarm_data["date"]:
                print("Alarm!")
    
    # Daily Repeats
    if "1" in alarms.keys():
        for alarm_data in alarms["1"]:
            if timeNow == alarm_data["time"]:
                print("Alarm!")

    #weekly Reapeats
    if "2" in alarms.keys():
        weekday = dtNow.weekday()
        for alarm_data in alarms["2"]:
            if timeNow == alarm_data["time"] and (datetime.datetime.strptime(str(alarm_data["date"]), '%Y-%m-%d').weekday()) == weekday:
                print("Alarm!")
    
    #Monthly Repeats
    if "3" in alarms.keys():
        for alarm_data in alarms["3"]:
            if timeNow == alarm_data["time"] and dateNow.split('-')[2] == str(alarm_data["date"]).split('-')[2]:
                print("Alarm!")

    #Yearly Repeats
    if "4" in alarms.keys():
        for alarm_data in alarms["4"]:
            if timeNow == alarm_data["time"] and dateNow.split('-')[2] == str(alarm_data["date"]).split('-')[2] and dateNow.split('-')[1] == str(alarm_data["date"]).split('-')[1]:
                print("Alarm!")

        
    
def message_parser(request_recieved):
    
    global process_pid_val
    global dtNow
    sender = request_recieved.split('|')[0]
    message = request_recieved.split('|')[1]
    if message == "STATUS":
        send_response_for_request("UP with Process ID->"+str(process_pid_val)) 
    elif message == "TIME":
        send_response_for_request(dtNow.time().strftime("%H:%M"))
    elif message == "NOW":
        response = "date: " + str(dtNow.date()) + " time: " + str(dtNow.time().strftime("%H:%M"))
        send_response_for_request(response)    
    elif message == "SET_ALARM":
        set_alarm()
    else:
        print("Invalid request")
        time.sleep(5)
        send_response_for_request("Invalid Request")    

    
def send_response_for_request(message):
    message = "[Clock Daemon]@ "+message
    viki_socket.send(message.encode('ascii'),zmq.NOBLOCK)

def check_for_requests():
    # Loop and accept messages from both channels, acting accordingly
    socks = dict(poller.poll(4000))
    if socks:
        if socks.get(viki_socket) == zmq.POLLIN:
            # Get request
            request_recieved = (viki_socket.recv(zmq.NOBLOCK)).decode()
            print(request_recieved)
            if request_recieved:
                if request_recieved.split('|')[1] == "[TERMINATE]":
                    send_response_for_request("Terminating daemon")
                    keepTime = False
                    return True
                else:    
                    message_parser(request_recieved)
                    return False
    else:
        print ("[INFO] Message timeout")
        return False

if __name__ == "__main__":

    #get_data_from_user()

    #timeKeeper()

    # if os.geteuid() != 0:
    #     print("[ERROR] Please run as root user!")
    #     exit()

    process_pid_val = os.getpid()
    print("[CTRL] Started Clock Daemon")
    end_process = False
    load_alarams()
    timeKeeperThread = threading.Thread(target=timeKeeper)
    timeKeeperThread.start()
    print("started thread!")
    #scan_port()
    while not end_process:
        end_process = check_for_requests()

    print("[CTRL] Terminating clock Daemon")
    keepTime = False
    timeKeeperThread.join()
    viki_socket.close()            